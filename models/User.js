const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required'],
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required'],
	},
	email: {
		type: String,
		required: [true, 'Email is required'],
	},
	password: {
		type: String,
		required: [true, 'Password is required'],
	},
	mobileNo: {
		type: Number,
		required: [true, 'Mobile number is required'],
	},
	isAdmin: {
		type: Boolean,
		default: false,
	},
	cart: [
		{
			productId: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Product',
				required: [true, 'Product Id is required.'],
			},

			name: {
				type: String,
				required: [true, 'Product Name is required!'],
			},

			price: {
				type: Number,
				required: [true, 'Product Price is required!'],
			},

			quantity: {
				type: Number,
				required: [true, 'Product Quantity is required!'],
			},

			subTotal: {
				type: Number,
			},
		},
	],
});

module.exports = mongoose.model('User', userSchema);
