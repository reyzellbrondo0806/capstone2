const mongoose = require('mongoose');
const Product = require('../models/Product');

const orderSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: [true, 'User ID is required'],
	},
	orderItems: [
		{
			productName: {
				type: String,
				ref: 'Product',
				required: [true, 'Product name is required'],
			},

			productId: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Product',
				required: [true, 'Product ID is required'],
			},

			quantity: {
				type: Number,
				required: [true, 'Quantity is required'],
			},
		},
	],
	totalAmount: {
		type: Number,
	},
	purchasedOn: {
		type: Date,
		default: new Date(),
	},
	status: {
		type: String,
		enum: [
			'Pending',
			'Shipped',
			'Delivered',
			'Received',
			'Completed',
			'Cancelled',
			'Returned',
		],
		default: 'Pending',
	},
});

module.exports = mongoose.model('Order', orderSchema);
