const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, 'Course is required'],
		},
		description: {
			type: String,
			required: [true, 'Description is required'],
		},
		price: {
			type: Number,
			required: [true, 'Price is required'],
		},
		stocks: {
			type: Number,
			required: [true, 'Number of stocks is required'],
		},
		isActive: {
			type: Boolean,
			default: true,
		},
		createdOn: {
			type: Date,
			default: new Date(),
		},
		// productType: {
		// 	type: String,
		// 	enum: [
		// 		'mac',
		// 		'ipad',
		// 		'iphone',
		// 		'watch',
		// 		'music',
		// 		'tv&home',
		// 		'accessories',
		// 	], // Define allowed values
		// 	required: [true, 'Product type is required'],
		// },
	},
	{ validateBeforeSave: true }
);

//Model
module.exports = mongoose.model('Product', productSchema);
