// Dependencies
const express = require("express");
const productController = require("../controllers/productController");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();


// Routes

// Create product route (admin only)
router.post("/addProduct", verify, verifyAdmin,
	productController.addProduct)


// Retrieve all products (admin only)
router.get("/allProducts", verify, verifyAdmin, productController.getAllProducts)


// Retrieve all "active" products
router.get("/activeProducts", productController.getAllActiveProducts)


// Retrieve single product
router.get("/:productId", productController.getProduct)


// Update product information (admin only)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct)


// Archive product (admin only)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct)


// Activate product (admin only)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct)

//[SECTION] Route for Search Course by Name
router.post('/searchByName', productController.searchProductsByName);	


module.exports = router;