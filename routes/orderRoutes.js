const express = require('express');
const orderController = require('../controllers/orderController');
const auth = require('../auth');
const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();

//User checkout (create order)
router.post('/checkout', verify, orderController.checkout);

//Retrieve autenticated user's orders
router.get('/userOrder', verify, orderController.getOrders);

//Retrieve all orders (ADMIN)
router.get('/all-orders', verify, verifyAdmin, orderController.getAllOrders);

//Export Route System
module.exports = router;
