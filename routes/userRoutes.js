const express = require("express");
const userController = require("../controllers/userController")
const auth = require("../auth");
const { verify, verifyAdmin } = auth;


const router = express.Router();



router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//User Authentication

router.post("/login",userController.loginUser);


// Retrieve User Details

router.get("/details", verify, userController.getProfile)


// Update user as admin
router.put('/updateAdmin', verify, verifyAdmin, userController.updateUserAsAdmin);





module.exports = router;