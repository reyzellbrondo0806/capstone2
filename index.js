// Dependencies and Modules

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//Routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes")



// Environment Setup
const port = 4001;


// Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());


// Database Connection
	mongoose.connect("mongodb+srv://reyzellbrondo0806:8qUqjnQO03RAAgLE@cluster0.p0jj1df.mongodb.net/capstone2?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

	//prompt
	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));

	//[Backend Routes]
	//http://localhost:3000/users
	app.use("/users",userRoutes);
	app.use("/products",productRoutes);
	app.use("/orders",orderRoutes)



//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}


module.exports = {app,mongoose};



