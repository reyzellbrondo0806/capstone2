const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');
const mongoose = require('mongoose');
const auth = require('../auth');

// User checkout (create order)
module.exports.checkout = async (req, res) => {
	try {
		const user = req.user; // Assuming you have user data available in req.user after authentication

		if (user.isAdmin) {
			return res.status(403).json({
				error:
					'This feature is not available for admin users. Please use a customer account.',
			});
		}

		let totalAmount = 0;
		let orderProducts = [];

		const { productId, quantity } = req.body;

		let filter = { _id: productId };
		let update = { $inc: { stocks: -parseInt(quantity) } };

		// Use findOneAndUpdate to get a Mongoose document
		var updatedProduct = await Product.findOneAndUpdate(filter, update, {
			new: true,
		});

		if (!updatedProduct) {
			// Handle the case where the product with the specified ID doesn't exist
			return res.status(400).json({ error: `Product can't be updated` });
		} else {
			// Handle the updated product
			totalAmount += updatedProduct.price * parseInt(quantity);

			orderProducts.push({
				productName: updatedProduct.name,
				productId: updatedProduct._id,
				quantity: quantity,
			});

			const newOrder = new Order({
				userId: user.id,
				orderItems: orderProducts,
				totalAmount: totalAmount,
				purchasedOn: new Date(),
			});

			if (await newOrder.save()) {
				return res
					.status(201)
					.json({ message: 'Order created successfully.', order: newOrder });
			} else {
				throw new Error('An error occurred while creating the order.');
			}
		}
	} catch (err) {
		// In this block, use updatedProduct to check if it's modified
		if (updatedProduct && updatedProduct.isModified('stocks')) {
			// Revert the product update by adding back the deducted quantity
			await Product.findOneAndUpdate(filter, { $inc: { stocks: quantity } });
		}

		res
			.status(500)
			.json({ error: 'An error occurred while creating the order.' });
	}
};

//Retrieve autenticated user's orders
module.exports.getOrders = async (req, res) => {
	try {
		const userId = req.query.userId;

		// Retrieve orders for the authenticated user and populate the 'orderItems.productId' field with product details
		const orders = await Order.find({ userId })
			.populate({
				path: 'orderItems.productId',
				select: 'name price', // You can select specific fields from the product
			})
			.exec();

		// Calculate subtotal and quantity for each order
		const ordersWithDetails = orders.map((order) => {
			const orderItems = order.orderItems;
			const subtotal = orderItems.reduce((total, item) => {
				return total + item.productId.price * item.quantity;
			}, 0);
			const quantity = orderItems.reduce((totalQuantity, item) => {
				return totalQuantity + item.quantity;
			}, 0);

			return {
				_id: order._id,
				orderItems: orderItems.map((item) => ({
					product: {
						_id: item.productId._id,
						name: item.productId.name,
						price: item.productId.price,
					},
					quantity: item.quantity,
				})),
				subtotal,
				quantity,
				purchasedOn: order.purchasedOn,
			};
		});

		res.status(200).json({ orders: ordersWithDetails });
	} catch (error) {
		res
			.status(500)
			.json({ message: 'An error occurred while retrieving orders', error });
	}
};

//Retrieve all orders (ADMIN)
module.exports.getAllOrders = async (req, res) => {
	try {
		if (!req.user.isAdmin) {
			return res
				.status(403)
				.json({ message: 'Access denied. Only admin users are allowed.' });
		}

		const orders = await Order.find();

		res.status(200).json(orders);
	} catch (error) {
		res.status(500).json({ message: 'An error occurred.', error });
	}
};
