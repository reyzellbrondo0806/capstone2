// Dependencies and Modules
const Product = require('../models/Product');

// Create a product
module.exports.addProduct = (req, res) => {
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		stocks: req.body.stocks,
		// productType: req.body.productType,
	});

	return newProduct
		.save()
		.then((product) => {
			res.send({
				name: product.name,
				quantity: product.quantity,
				price: product.price,
				stock: product.stock,
			});
		})
		.catch((err) => res.status(500).send(err));
};

// Retrieve all products
module.exports.getAllProducts = async (req, res) => {
	try {
		const page = parseInt(req.query.page) || 1;
		const perPage = 5;
		const skip = (page - 1) * perPage;

		const products = await Product.find().skip(skip).limit(perPage);

		const totalProducts = await Product.countDocuments();

		const totalPages = Math.ceil(totalProducts / perPage);

		res.status(200).json({
			products,
			currentPage: page,
			totalPages: totalPages,
		});
	} catch (err) {
		console.log(err);
		res.status(500).json({ error: 'An error occured' });
	}
};

// Retrieve all "active" products
module.exports.getAllActiveProducts = async (req, res) => {
	try {
		const page = parseInt(req.query.page) || 1;
		const perPage = 5;
		const skip = (page - 1) * perPage;

		const products = await Product.find({ isActive: true })
			.skip(skip)
			.limit(perPage);

		// Count total products for pagination metadata
		const totalProducts = await Product.countDocuments();

		// Calculate total pages
		const totalPages = Math.ceil(totalProducts / perPage);

		res.status(200).json({
			products,
			currentPage: page,
			totalPages: totalPages,
		});
	} catch (err) {
		console.log(err);
		res.status(500).json({ error: 'An error occured' });
	}
};

// Retrieve single product
module.exports.getProduct = (req, res) => {
	return Product.findById(req.params.productId).then((result) => {
		return res.send(result);
	});
};

// Update product information (admin only)
module.exports.updateProduct = (req, res) => {
	console.log(req.body);
	let updatedProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		stocks: req.body.stocks,
	};

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then(
		(product, error) => {
			if (error) {
				return res.send(false);
			} else {
				return res.send(true);
			}
		}
	);
};

// Archive product (admin only)
module.exports.archiveProduct = (req, res) => {
	let updateActiveField = {
		isActive: false,
	};

	return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
		.then((product, error) => {
			//course archived successfully
			if (error) {
				return res.send(false);

				// failed
			} else {
				return res.send(true);
			}
		})
		.catch((err) => res.send(err));
};

// Activate product (admin only)
module.exports.activateProduct = (req, res) => {
	let updateActiveField = {
		isActive: true,
	};

	return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
		.then((product, error) => {
			//course archived successfully
			if (error) {
				return res.send(false);

				// failed
			} else {
				return res.send(true);
			}
		})
		.catch((err) => res.send(err));
};

//ChatGPT Generated Code

module.exports.searchProductsByName = async (req, res) => {
	try {
		const { name } = req.body;
		const page = parseInt(req.query.page) || 1;
		const perPage = 5;
		const skip = (page - 1) * perPage;

		// Use a regular expression to perform a case-insensitive search
		const products = await Product.find({
			$and: [{ name: { $regex: name, $options: 'i' } }, { isActive: true }],
		})
			.skip(skip)
			.limit(perPage);

		// Count total products for pagination metadata (including inactive products)
		const totalProducts = await Product.countDocuments({
			name: { $regex: name, $options: 'i' },
		});

		// Calculate total pages
		const totalPages = Math.ceil(totalProducts / perPage);

		res.status(200).json({
			products,
			currentPage: page,
			totalPages,
		});
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: 'Internal Server Error' });
	}
};
