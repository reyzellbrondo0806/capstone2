const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.registerUser = (reqbody) => {
	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		mobileNo: reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password, 10),
	});

	return newUser
		.save()
		.then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
		.catch((err) => err);
};

module.exports.loginUser = (req, res) => {
	return User.findOne({ email: req.body.email })
		.then((result) => {
			if (result === null) {
				return res.status(401).send(false);
			} else {
				//compareSync method is used to compare a non-encrypted password and from the encrypted password from the db
				const isPasswordCorrect = bcrypt.compareSync(
					req.body.password,
					result.password
				);
				//if pw match
				if (isPasswordCorrect) {
					return res.status(200).send({
						id: result._id,
						isAdmin: result.isAdmin,
						access: auth.createAccessToken(result),
					});
				}
				//else pw does not match
				else {
					return res.status(401).send(false);
				}
			}
		})
		.catch((err) => {
			console.log(err);
			res.status(500).send(err);
		});
};

// Retrieve user details
module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id)
		.then((result) => {
			result.password = '';
			return res.send(result);
		})
		.catch((err) => res.send(err));
};

module.exports.updateUserAsAdmin = async (req, res) => {
	try {
		const { userId } = req.body;

		// Find the user and update isAdmin flag
		const user = await User.findById(userId);

		if (!user) {
			return res.status(404).json({ error: 'User not found' });
		}

		user.isAdmin = true;

		// Save the updated user document
		await user.save();

		res.status(200).json({ message: 'User updated as admin successfully' });
	} catch (error) {
		res
			.status(500)
			.json({ error: 'An error occurred while updating the user as admin' });
	}
};
